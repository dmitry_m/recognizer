package com.example.ns.recognizer.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.ns.recognizer.Camera.CameraPreview;
import com.example.ns.recognizer.R;
import com.example.ns.recognizer.Recognize;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.FileManager;
import com.example.ns.recognizer.Utils.Utils;

import java.io.IOException;


public class MainActivity extends CameraHandlingActivity {
    private CameraPreview cameraPreview;
    private FileManager fileManager = new FileManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Config.photoCapture) {
            setupCameraPreview();
        } else {
            recognize();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setupCameraPreview() {
        setupPictureCallback();

        cameraPreview = new CameraPreview(this, camera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
        preview.addView(cameraPreview);
    }

    private void setupPictureCallback() {
        Button captureButton = (Button) findViewById(R.id.captureButton);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        camera.takePicture(null, null, getPictureCallback());
                        camera.startPreview();
                    }
                }
        );
    }

    private Camera.PictureCallback getPictureCallback() {
        return new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                try {
                    startResultViewActivity();
                    saveBitmap(data);
                    recognize();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void startResultViewActivity() {
        Intent intent = new Intent(this, ResultView.class);
        startActivity(intent);
    }

    private void saveBitmap(byte[] data) throws IOException {
        String generatedName = Utils.generatePhotoName();
        Config.setWorkName(generatedName);
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        bitmap = Utils.adjustBitmap(bitmap);
        fileManager.saveBitmap(bitmap, Config.getAdjustedFilename());
    }

    private void recognize() {
        try {
            Recognize recognize = new Recognize();
            recognize.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
