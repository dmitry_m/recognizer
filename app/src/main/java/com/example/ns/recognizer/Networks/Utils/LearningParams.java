package com.example.ns.recognizer.Networks.Utils;

import com.example.ns.recognizer.Utils.Config;

/**
 * Created by dima on 28.02.2015.
 */
public class LearningParams {
    public int cellX;
    public int cellY;
    public double underestimateErrorMultiplier;
    public double overestimateErrorMultiplier;
    public double growthCoefficientStart;
    public double growthCoefficientMultiplier;
    public double growthCoefficientBottomLimit;

    public LearningParams() {
        this(Config.cellX,
                Config.cellY,
                Config.underestimateErrorMultiplier,
                Config.overestimateErrorMultiplier,
                Config.growthCoefficientStart,
                Config.growthCoefficientMultiplier,
                Config.growthCoefficientBottomLimit);
    }

    public LearningParams(int cellX,
                          int cellY,
                          double underestimateMultiplier,
                          double overestimateMultiplier) {
        this(cellX, cellY, underestimateMultiplier, overestimateMultiplier, Config.growthCoefficientStart, Config.growthCoefficientMultiplier, Config.growthCoefficientBottomLimit);
    }

    public LearningParams(int cellX,
                          int cellY,
                          double underestimateMultiplier,
                          double overestimateMultiplier,
                          double growthCoefficientStart,
                          double growthCoefficientMultiplier,
                          double growthCoefficientBottomLimit) {
        this.cellX = cellX;
        this.cellY = cellY;
        this.underestimateErrorMultiplier = underestimateMultiplier;
        this.overestimateErrorMultiplier = overestimateMultiplier;
        this.growthCoefficientStart = growthCoefficientStart;
        this.growthCoefficientMultiplier = growthCoefficientMultiplier;
        this.growthCoefficientBottomLimit = growthCoefficientBottomLimit;
    }

    public void apply() {
        Config.cellX = cellX;
        Config.cellY = cellY;
        Config.underestimateErrorMultiplier = underestimateErrorMultiplier;
        Config.overestimateErrorMultiplier = overestimateErrorMultiplier;
        Config.growthCoefficientStart = growthCoefficientStart;
        Config.growthCoefficientMultiplier = growthCoefficientMultiplier;
        Config.growthCoefficientBottomLimit = growthCoefficientBottomLimit;
    }
}
