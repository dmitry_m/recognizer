package com.example.ns.recognizer;

import com.example.ns.recognizer.Filter.Filter;
import com.example.ns.recognizer.Networks.NetworkManager;
import com.example.ns.recognizer.Segment.Objects.Findings;
import com.example.ns.recognizer.Segment.Segmenter;
import com.example.ns.recognizer.Utils.Config;

/**
 * Created by dima on 26.01.2015.
 */
public class Recognize {
    private final Filter filter = new Filter();
    private final Segmenter segmenter = new Segmenter();
    private final NetworkManager networkManager = new NetworkManager();

    public void execute() throws Exception {
        System.out.println("###d execute");

        filter.filter();

        Findings findings = segmenter.segment();

        if (!Config.onlySegmentation) {
            networkManager.recognize(findings);
        }

        System.out.println("###d done");
    }
}
