package com.example.ns.recognizer.Networks.Neurons;

import android.graphics.Rect;

import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dima on 07.02.2015.
 */
public class TailsNeuron implements Serializable{
    private Tail[] tails = new Tail[0];

    private boolean notSet = true;
    private char symbol;

    public int react(SymbolArea area) {
        if (!canReactTo(area)) {
            return Config.unsuitableTailScores;
        }

        int reaction = 0;
        for (Tail tail : this.tails) {
            int closestTailIndex = getClosestTailIndex(area, tail);
            reaction += tail.react(area, closestTailIndex);
        }

        return reaction;
    }

    public void evolve(SymbolArea area, char symbol) {
        if (!canEvolveTo(area)) {
            return;
        }

        if (notSet) {
            initTails(area);
            this.symbol = symbol;
            notSet = false;
        } else {
            for (Tail tail : this.tails) {
                int closestTailIndex = getClosestTailIndex(area, tail);
                tail.evolve(area, closestTailIndex);
            }
        }
    }

    private int getClosestTailIndex(SymbolArea area, Tail tail) {
        ArrayList<Rect> tailRects = area.getTails();

        int minIndex = 0;
        int min = tail.react(area, minIndex);
        for (int i = minIndex + 1; i < tailRects.size(); ++i) {
            int candidateMin = tail.react(area, i);
            if (min > candidateMin) {
                minIndex = i;
                min = candidateMin;
            }
        }
        return minIndex;
    }

    public boolean canEvolveTo(SymbolArea area) {
        return notSet || (this.tails.length == area.getTails().size());
    }

    public boolean canReactTo(SymbolArea area) {
        return canEvolveTo(area);
    }

    private void initTails(SymbolArea area) {
        ArrayList<Rect> tailRects = area.getTails();
        this.tails = new Tail[tailRects.size()];

        for (int i = 0; i < tailRects.size(); ++i) {
            this.tails[i] = new Tail(area, i);
        }
    }

    public char getSymbol() {
        return this.symbol;
    }

    public Tail[] getTails() {
        return this.tails;
    }
}
