package com.example.ns.recognizer.Segment.Objects;

import java.util.ArrayList;

/**
 * Describes hand-written line
 */

public class Line {
    private Limit limit;
    private Limit previousLimit;
    private final ArrayList<SymbolArea> areas;

    public Line(SymbolArea area) {
        this.limit = new Limit(area.top, area.bottom);
        this.previousLimit = new Limit(limit);

        this.areas = new ArrayList<SymbolArea>();
        this.areas.add(area);
    }

    public boolean covers(SymbolArea area) {
        Limit limit = (isIntersection(area) ? this.previousLimit : this.limit);
        return isInLine(area, limit);
    }

    public boolean isInLine(SymbolArea area, Limit limit) {
        return (area.top >= limit.top && area.top <= limit.bottom)
                || (area.bottom >= limit.top && area.bottom <= limit.bottom)
                || (area.top < limit.top && area.bottom > limit.bottom);
    }

    public void add(SymbolArea area) {
        SymbolArea areaToAdd = new SymbolArea(area);

        if (isIntersection(area)) {
            updateSymbolArea(area);
        }
        else {
            addSymbolArea(areaToAdd);
        }
    }

    private void adjustLimit(SymbolArea areaToAdd) {
        limit.adjust(areaToAdd.top, areaToAdd.bottom);
    }

    private boolean isIntersection(SymbolArea area) {
        SymbolArea previous = areas.get(areas.size() - 1);
        return area.left < previous.right;
    }

    private void updateSymbolArea(SymbolArea area) {
        SymbolArea previous = areas.get(areas.size() - 1);
        mergeIntoFirst(area, previous);

        areas.set(areas.size() - 1, area);
        adjustLimit(area);
    }

    private void addSymbolArea(SymbolArea area) {
        previousLimit = new Limit(limit);

        areas.add(area);
        adjustLimit(area);
    }

    private void mergeIntoFirst(SymbolArea lhs, SymbolArea rhs) {
        lhs.left = (lhs.left < rhs.left ? lhs.left : rhs.left);
        lhs.top = (lhs.top < rhs.top ? lhs.top : rhs.top);
        lhs.right = (lhs.right > rhs.right ? lhs.right : rhs.right);
        lhs.bottom = (lhs.bottom > rhs.bottom ? lhs.bottom : rhs.bottom);
    }

    public ArrayList<SymbolArea> getAreas() {
        return areas;
    }
}
