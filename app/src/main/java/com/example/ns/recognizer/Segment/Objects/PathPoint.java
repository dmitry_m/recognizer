package com.example.ns.recognizer.Segment.Objects;

import android.graphics.Point;

/**
 * Created by dima on 07.06.2015.
 */
public class PathPoint extends WeightedPoint {
    public PathPoint previous;

    public PathPoint(Point point, double weight) {
        super(point, weight);
    }

    public PathPoint(WeightedPoint weightedPoint) {
        super(weightedPoint);
    }

    public PathPoint(Point point, double weight, PathPoint previous) {
        super(point, weight);
        this.previous = previous;
    }

    public PathPoint(WeightedPoint weightedPoint, PathPoint previous) {
        super(weightedPoint);
        this.previous = previous;
    }

    public boolean hasPrevious() {
        return (null != previous);
    }
}
