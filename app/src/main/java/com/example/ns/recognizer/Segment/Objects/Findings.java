package com.example.ns.recognizer.Segment.Objects;

import java.util.ArrayList;

/**
 * Created by dima on 01.02.2015.
 */
public class Findings {
    private ArrayList<Line> lines;

    public Findings() {
        reset();
    }

    public void reset() {
        this.lines = new ArrayList<>();
    }

    public void add(SymbolArea area) {
        for (Line line : lines) {
            if (line.covers(area)) {
                line.add(area);
                return;
            }
        }

        Line line = new Line(area);
        lines.add(line);
    }

    public ArrayList<Line> getLines() {
        return lines;
    }
}
