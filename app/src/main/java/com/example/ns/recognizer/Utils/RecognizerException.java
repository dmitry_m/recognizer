package com.example.ns.recognizer.Utils;

/**
 * Created by dima on 22.03.2015.
 */
public class RecognizerException extends RuntimeException {
    public RecognizerException(String detailMessage) {
        super(detailMessage);
    }
}
