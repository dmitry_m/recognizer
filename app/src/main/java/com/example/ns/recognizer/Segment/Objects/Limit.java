package com.example.ns.recognizer.Segment.Objects;

/**
 * Created by dima on 03.02.2015.
 */


public class Limit {
    public int top;
    public int bottom;

    Limit(int top, int bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    Limit(Limit limit) {
        this(limit.top, limit.bottom);
    }

    void adjust(int top, int bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    void adjust(Limit limit) {
        adjust(limit.top, limit.bottom);
    }
}
