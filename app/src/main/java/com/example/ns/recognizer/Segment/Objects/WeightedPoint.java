package com.example.ns.recognizer.Segment.Objects;

import android.graphics.Point;

/**
 * Created by dima on 07.06.2015.
 */
public class WeightedPoint {
    public Point point;
    public double weight;

    public WeightedPoint() {
        this.point = new Point();
        this.weight = 0;
    }

    public WeightedPoint(Point point, double weight) {
        this.point = point;
        this.weight = weight;
    }

    public WeightedPoint(WeightedPoint weightedPoint) {
        this.point = new Point(weightedPoint.point);
        this.weight = weightedPoint.weight;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof WeightedPoint) {
            return point.equals(((WeightedPoint)o).point);
        }
        return false;
    }
}
