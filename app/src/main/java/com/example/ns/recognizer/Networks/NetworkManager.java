package com.example.ns.recognizer.Networks;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Networks.Kohonen.INetwork;
import com.example.ns.recognizer.Networks.Kohonen.Kohonen;
import com.example.ns.recognizer.Networks.Kohonen.KohonenPath;
import com.example.ns.recognizer.Networks.Kohonen.KohonenTails;
import com.example.ns.recognizer.Networks.Utils.ImagePrinter;
import com.example.ns.recognizer.Networks.Utils.LearningParams;
import com.example.ns.recognizer.Networks.Utils.LearningParamsGenerator;
import com.example.ns.recognizer.Segment.Objects.Findings;
import com.example.ns.recognizer.Segment.Objects.Line;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Solver.Solver;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.FileManager;
import com.example.ns.recognizer.Utils.RecognizerException;
import com.example.ns.recognizer.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by dima on 07.02.2015.
 */
public class NetworkManager {
    private final FileManager fileManager = new FileManager();
    private final LearningParamsGenerator learningParamsGenerator = new LearningParamsGenerator();
    private final Solver solver = new Solver();
    private final ImagePrinter imagePrinter = new ImagePrinter();
    private Kohonen imageNetwork;
    private KohonenTails tailsNetwork;
    private KohonenPath pathsNetwork;

    public NetworkManager() {
        reset();
    }

    public void reset() {
        imageNetwork = new Kohonen();
        tailsNetwork = new KohonenTails();
        pathsNetwork = new KohonenPath();

        if (!Config.learningMode) {
            imageNetwork.deserialize();
            tailsNetwork.deserialize();
            pathsNetwork.deserialize();
        }
    }

    public void recognize(Findings findings) throws Exception {
        System.out.println("###d networks started");

        Bitmap bitmap = fileManager.openBitmap(Config.getFilteredFilename());

        if (Config.learningMode) {
            learningProgram(findings, bitmap);
        }
        else {
            workProgram(findings, bitmap);
        }

        System.out.println("###d networks done");
    }

    private void workProgram(Findings findings, Bitmap bitmap) throws IOException {
        System.out.println("###d start work");

        reset();
        processImage(findings, bitmap);

        System.out.println("###d work's done");
    }

    private void learningProgram(Findings findings, Bitmap bitmap) throws Exception {
        System.out.println("###d start learning");

        for (LearningParams learningParams : getLearningParams()) {
            learningParams.apply();

            reset();
            startLearning(findings, bitmap, false);
        }

        System.out.println("###d learned");
    }

    private ArrayList<LearningParams> getLearningParams() {
        ArrayList<LearningParams> learningParamsArrayList = new ArrayList<>();

        learningParamsArrayList.add(new LearningParams());

        return learningParamsArrayList;
    }


    private void processImage(Findings findings, Bitmap bitmap) throws IOException {
        Bitmap processed = fileManager.openBitmap(Config.getAdjustedFilename()).copy(Bitmap.Config.ARGB_8888, true);
        //Bitmap processed = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        for (Line line : findings.getLines()) {
            String result = "";
            for (SymbolArea area : line.getAreas()) {
                result += recognizeSymbol(bitmap, area);
            }
            String answer = null;
            try {
                answer = solver.solve(result);
            } catch (RecognizerException e) {
                System.out.println(e.getMessage());
                continue;
            }

            System.out.println("Printing the answer: " + answer);
            imagePrinter.printAnswerOnImage(processed, line, answer, imageNetwork);
        }

        fileManager.saveBitmap(processed, Config.getProcessedFilename());
    }

    private char recognizeSymbol(Bitmap bitmap, SymbolArea area) {
        Bitmap resized = Utils.extractCellFromBitmap(bitmap, area);

        ArrayList<Integer> imageScores = imageNetwork.recognize(resized, area);
        ArrayList<Integer> tailScores = tailsNetwork.recognize(resized, area);
        ArrayList<Integer> pathScores = pathsNetwork.recognize(resized, area);

        return getBestSymbolForScores(pathsNetwork, pathScores);
    }

    private char getBestSymbolForScores(INetwork network, ArrayList<Integer> scores) {
        int minIndex = 0;
        int min = scores.get(minIndex);
        for (int i = minIndex + 1; i < scores.size(); ++i) {
            if (min > scores.get(i)) {
                minIndex = i;
                min = scores.get(i);
            }
        }
        return network.getSymbolByNeuronIndex(minIndex);
    }

    private void startLearning(Findings findings, Bitmap bitmap, boolean onlyNotOks) throws Exception {
        int totalLines = 27;
        final int times = totalLines * Config.learnTimes;
        for (int j = 0; j < times; ++j) {
            int lineIndex = (j % totalLines);
            Line line = findings.getLines().get(lineIndex);

            String timeString = "--- Time " + j + " (line " + lineIndex + ") --------------------------------------------------";
            System.out.println(timeString + timeString + timeString + timeString);

            int areas = line.getAreas().size();
            if (areas != Config.learningChars.length) {
                SymbolArea area = line.getAreas().get(0);
                System.out.println(area.centerX() + "x" + area.centerY());

                throw new Exception("Learning error: not that amount of areas that expected (" + areas + ", but expected " + Config.learningChars.length + ")");
            }

            for (int i = 0; i < areas; ++i) {
                SymbolArea area = line.getAreas().get(i);

                char learningChar = Config.getLearningChar(i);
                System.out.print("Char " + learningChar + ". ");
                imageNetwork.learn(bitmap, area, learningChar);
                tailsNetwork.learn(bitmap, area, learningChar);
                pathsNetwork.learn(bitmap, area, learningChar);
            }
        }

        imageNetwork.serialize();
        tailsNetwork.serialize();
        pathsNetwork.serialize();

        printScoresIfDebug(pathsNetwork, bitmap, totalLines, findings, onlyNotOks);
    }

    private void printScoresIfDebug(INetwork network, Bitmap bitmap, int totalLines, Findings findings, boolean onlyNotOks) throws Exception {
        if (!Config.debugNetworkManager) {
            return;
        }

        int notOks = 0;
        for (int i = 0; i < totalLines; ++i) {
            if (!onlyNotOks) {
                System.out.println("--- Time " + i + " --------------------------------------------------------------------");
                network.printNeuronsHeaders();
            }

            Line line = findings.getLines().get(i);
            for (int j = 0; j < line.getAreas().size(); ++j) {
                SymbolArea area = line.getAreas().get(j);
                char symbol = Config.getLearningChar(j);

                Bitmap resized = Utils.extractCellFromBitmap(bitmap, area);
                ArrayList<Integer> scores = network.recognize(resized, area);

                final int minIndex = getMinIndex(scores);

                String resultString = "";
                if (!onlyNotOks) {
                    for (int k = 0; k < scores.size(); ++k) {
                        resultString += String.format("%2d", scores.get(k) / 1000) + (minIndex == k ? "*" : " ") + " ";
                    }
                }

                final boolean minIndexOk = (network.getSymbolByNeuronIndex(minIndex) == symbol);
                if (!minIndexOk) {
                    notOks++;
                }

                if (!onlyNotOks) {
                    final String minIndexOkStr = (minIndexOk ? "[  ok  ]" : "[not ok]");
                    final String minStr = "  " + String.format("[ %5d ]", scores.get(minIndex));
                    System.out.println("Digit \"" + symbol + "\": " + resultString + minIndexOkStr + minStr);
                }
            }
        }
        if (!onlyNotOks) {
            System.out.println("---------------------------------------------------------------------------");
        }

        String line = "Learning params: " + Config.cellX + "x" + Config.cellY + "; "
                + Config.underestimateErrorMultiplier + ", " + Config.overestimateErrorMultiplier + "; "
                + Config.growthCoefficientStart + ", " + Config.growthCoefficientMultiplier + ", " + Config.growthCoefficientBottomLimit;
        line += ".  not oks: " + notOks + "/" + (totalLines * 10);
        System.out.println(line);
    }

    private int getMinIndex(ArrayList<Integer> scores) {
        int min = scores.get(0);
        int minIndex = 0;
        for (int i = 1; i < scores.size(); ++i) {
            if (scores.get(i) < min) {
                min = scores.get(i);
                minIndex = i;
            }
        }
        return minIndex;
    }
}
