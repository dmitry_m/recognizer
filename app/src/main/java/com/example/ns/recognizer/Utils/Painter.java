package com.example.ns.recognizer.Utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.ns.recognizer.Segment.Objects.Area;
import com.example.ns.recognizer.Segment.Objects.Findings;
import com.example.ns.recognizer.Segment.Objects.Line;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;

public class Painter {
    private final int[] colors = {Color.RED, Color.GREEN, Color.MAGENTA, Color.BLUE, Color.CYAN, Color.YELLOW};
    private final Paint paint;
    private final Paint tailsPaint;

    public Painter() {
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(Config.segmentBordersWidth);

        tailsPaint = new Paint();
        tailsPaint.setStyle(Paint.Style.STROKE);
        tailsPaint.setStrokeWidth(Config.tailsBordersWidth);
    }

    public Bitmap drawBorders(Bitmap bitmap, Findings findings) {
        Canvas canvas = new Canvas(bitmap);

        int index = 0;
        for (Line line : findings.getLines()) {
            prepareColor(index++);

            for (SymbolArea area : line.getAreas()) {
                drawArea(canvas, area);
            }
        }

        return bitmap;
    }

    private void drawArea(Canvas canvas, SymbolArea area) {
        drawBorder(canvas, area);
        drawTails(canvas, area);
    }

    private void drawBorder(Canvas canvas, Area area) {
        Rect border = area.toRect();
        border.left -= Config.segmentBordersWidth;
        border.top -= Config.segmentBordersWidth;
        border.right += Config.segmentBordersWidth;
        border.bottom += Config.segmentBordersWidth;
        canvas.drawRect(border, paint);
    }

    private void drawTails(Canvas canvas, SymbolArea area) {
        for (Rect tail : area.getTails()) {
            canvas.drawRect(tail, tailsPaint);
        }
    }

    private void prepareColor(int index) {
        int color = colors[index % colors.length];
        paint.setColor(color);

        int tailsColor = colors[(index + 1) % colors.length];
        tailsPaint.setColor(tailsColor);
    }
}