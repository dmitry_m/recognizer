package com.example.ns.recognizer.Segment.Objects;

/**
 * Created by dima on 27.05.2015.
 */
public class TailSearchResult {
    public boolean success;
    public int longestBorder;

    public TailSearchResult() {
        this.success = false;
        this.longestBorder = 0;
    }

    public TailSearchResult(int longestBorder) {
        this.success = true;
        this.longestBorder = longestBorder;
    }
}
