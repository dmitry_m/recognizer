package com.example.ns.recognizer.Segment.Objects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by dima on 31.01.2015.
 */
public class Mask {
    private Bitmap bitmap;
    private Canvas canvas;
    private Paint paint;

    public void reset(Bitmap bitmap) {
        this.bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        this.canvas = new Canvas(this.bitmap);

        this.paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(1);
        paint.setColor(Color.GREEN);
    }

    public void use(final Rect rect) {
        canvas.drawRect(rect, paint);
    }

    public void use(final SymbolArea area) {
        use(area.toRect());
    }

    public boolean isUsed(final Rect rect) {
        return (Color.GREEN == bitmap.getPixel(rect.left, rect.top)
                || Color.GREEN == bitmap.getPixel(rect.right, rect.top)
                || Color.GREEN == bitmap.getPixel(rect.right, rect.bottom)
                || Color.GREEN == bitmap.getPixel(rect.left, rect.bottom));
    }

    public boolean isUsed(final SymbolArea area) {
        return isUsed(area.toRect());
    }
}
