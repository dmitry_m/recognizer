package com.example.ns.recognizer.Segment.Objects;

import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by dima on 21.05.2015.
 */
public class SymbolArea extends Area {
    private ArrayList<Rect> tails = new ArrayList<>();
    private ArrayList<Double> angles;

    public SymbolArea() {
    }

    public SymbolArea(SymbolArea other) {
        super(other);
        this.tails = new ArrayList<>(other.getTails());
    }

    public SymbolArea(int left, int top, int right, int bottom) {
        super(left, top, right, bottom);
    }

    public SymbolArea(Area area) {
        super(area);
    }

    public void addTail(Rect rect) {
        tails.add(rect);
    }

    public ArrayList<Rect> getTails() {
        return tails;
    }

    public void setAngles(ArrayList<Double> path) {
        this.angles = path;
    }

    public ArrayList<Double> getAngles() {
        return angles;
    }
}
