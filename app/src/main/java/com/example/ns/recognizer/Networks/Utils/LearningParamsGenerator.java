package com.example.ns.recognizer.Networks.Utils;

import java.util.ArrayList;

public class LearningParamsGenerator {
    private ArrayList<LearningParams> generateLearningParams() {
        ArrayList<Integer> sizes = new ArrayList<Integer>();
        sizes.add(10);
        sizes.add(12);
        sizes.add(14);
        sizes.add(16);

        ArrayList<Integer> multipliers = new ArrayList<Integer>();
        multipliers.add(1);
        multipliers.add(2);
        multipliers.add(3);

        ArrayList<Double> growthCoefficientStartValues = new ArrayList<Double>();
        growthCoefficientStartValues.add(1.0);
        growthCoefficientStartValues.add(0.9);
        growthCoefficientStartValues.add(0.8);
        growthCoefficientStartValues.add(0.7);

        ArrayList<Double> growthCoefficientMultiplierValues = new ArrayList<Double>();
        growthCoefficientMultiplierValues.add(0.9);
        growthCoefficientMultiplierValues.add(0.8);
        growthCoefficientMultiplierValues.add(0.7);
        growthCoefficientMultiplierValues.add(0.6);

        ArrayList<Double> growthCoefficientBottomLimitValues = new ArrayList<Double>();
        growthCoefficientBottomLimitValues.add(0.1);
        growthCoefficientBottomLimitValues.add(0.2);

        return generateLearningParamsByLists(sizes, multipliers, growthCoefficientStartValues, growthCoefficientMultiplierValues, growthCoefficientBottomLimitValues);
    }

    private ArrayList<LearningParams> generateLearningParamsByLists(ArrayList<Integer> sizes,
                                                                    ArrayList<Integer> multipliers,
                                                                    ArrayList<Double> growthCoefficientStartValues,
                                                                    ArrayList<Double> growthCoefficientMultiplierValues,
                                                                    ArrayList<Double> growthCoefficientBottomLimitValues) {
        int size = sizes.size() * sizes.size() * multipliers.size() * multipliers.size() * growthCoefficientStartValues.size() * growthCoefficientMultiplierValues.size() * growthCoefficientBottomLimitValues.size();
        ArrayList<LearningParams> learningParamsArrayList = new ArrayList<LearningParams>(size);

        for (int sizeX : sizes) {
            for (int sizeY : sizes) {
                for (int underestimateMultiplier : multipliers) {
                    for (int overestimateMultiplier : multipliers) {
                        for (double growthCoefficientStart : growthCoefficientStartValues) {
                            for (double growthCoefficientMultiplier : growthCoefficientMultiplierValues) {
                                for (double growthCoefficientBottomLimit : growthCoefficientBottomLimitValues) {
                                    learningParamsArrayList.add(
                                            new LearningParams(sizeX,
                                                    sizeY,
                                                    underestimateMultiplier,
                                                    overestimateMultiplier,
                                                    growthCoefficientStart,
                                                    growthCoefficientMultiplier,
                                                    growthCoefficientBottomLimit));
                                }
                            }
                        }
                    }
                }
            }
        }

        return learningParamsArrayList;
    }
}