package com.example.ns.recognizer.Networks.Kohonen;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Segment.Objects.SymbolArea;

import java.util.ArrayList;

/**
 * Created by dima on 14.02.2015.
 */
public interface INetwork {
    void reset();

    void learn(Bitmap bitmap, SymbolArea area, char symbol);

    ArrayList<Integer> recognize(Bitmap bitmap, SymbolArea area);

    void printNeuronsHeaders();

    char getSymbolByNeuronIndex(int index);

    void serialize();

    void deserialize();
}
