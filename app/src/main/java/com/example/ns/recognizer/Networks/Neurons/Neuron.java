package com.example.ns.recognizer.Networks.Neurons;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Utils;

import java.io.Serializable;

/**
 * Created by dima on 07.02.2015.
 */
public class Neuron implements Serializable{
    private int[][] weights;

    private double growthCoefficient;
    private boolean notSet;
    private char symbol;
    public Neuron() {
        reset();
    }

    public void reset() {
        this.weights = new int[Config.cellX][Config.cellY];

        for (int x = 0; x < Config.cellX; ++x) {
            for (int y = 0; y < Config.cellY; ++y) {
                this.weights[x][y] = Config.defaultWeight;
            }
        }

        growthCoefficient = Config.growthCoefficientStart;
        notSet = true;
    }

    public int react(Bitmap bitmap) {
        if (notSet) {
            return 0;
        }

        int reaction = 0;

        int pluses = 0;
        int plusesTotal = 0;

        int minuses = 0;
        int minusesTotal = 0;
        for (int x = 0; x < Config.cellX; ++x) {
            for (int y = 0; y < Config.cellY; ++y) {
                int weight = this.weights[x][y];

                int pixelPower = Utils.getPixelPower(bitmap, x, y);
                int linkReaction = Math.abs(pixelPower - weight);

                if (pixelPower >= weight) {
                    reaction += (Config.underestimateErrorMultiplier * linkReaction);
                    pluses += (Config.underestimateErrorMultiplier * linkReaction);
                    plusesTotal += 1;
                }
                else {
                    reaction += (Config.overestimateErrorMultiplier * linkReaction);
                    minuses += (Config.overestimateErrorMultiplier * linkReaction);
                    minusesTotal += 1;
                }
            }
        }

        if (Config.debugNeuron)
            System.out.println((int)pluses + "(" + plusesTotal + "), " + (int)minuses + "(" + minusesTotal + "):");

        return reaction;
    }

    public void evolve(Bitmap bitmap, char symbol) {
        printWeightsIfDebug();

        for (int x = 0; x < Config.cellX; ++x) {
            for (int y = 0; y < Config.cellY; ++y) {
                int pixelPower = Utils.getPixelPower(bitmap, x, y);
                int weight = this.weights[x][y];

                double delta = (pixelPower - weight) * growthCoefficient;

                weights[x][y] += (int)delta;
            }
        }

        if (notSet) {
            this.symbol = symbol;
            notSet = false;
        }

        updateGrowthCoefficient();
        printWeightsIfDebug();
    }

    private void updateGrowthCoefficient() {
        if (growthCoefficient > Config.growthCoefficientBottomLimit) {
            growthCoefficient *= Config.growthCoefficientMultiplier;
        }
    }

    public void printWeightsIfDebug() {
        if (!Config.debugNeuron) {
            return;
        }

        printWeights();
    }

    public void printWeights() {
        for (int y = 0; y < Config.cellY; ++y) {
            String line = "";
            for (int x = 0; x < Config.cellX; ++x) {
                line += String.format("%3d ", weights[x][y]);
            }
            System.out.println(line);
        }
    }

    public char getSymbol() {
        return symbol;
    }

    public int[][] getWeights() {
        return weights;
    }
}
