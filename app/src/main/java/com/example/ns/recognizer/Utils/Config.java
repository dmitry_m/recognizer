package com.example.ns.recognizer.Utils;

import android.os.Environment;

/**
 * Created by dima on 01.02.2015.
 */
public class Config {
    // turn off if you want to automatically recognize image with <workName> name
    public final static boolean photoCapture = false;

    // for testing purposes
    public final static boolean onlySegmentation = false;

    // checks
    public final static boolean tailCheck = true;
    public final static boolean pathCheck = true;

    // in learning mode <learningName> is to be used else <workName>
    public final static boolean learningMode = true;
    public final static int learnTimes = 1;
    public final static char[] learningChars = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '*', '/', '+', '=', '-' };

    public final static int radius = 2;
    public final static int diameter = radius * 2;
    public final static int stepX = 1;
    public final static int stepY = 1;
    public static int cellX = 16;
    public static int cellY = 16;
    public static double sectorXFactor = 2.5;
    public static double sectorYFactor = 3;
    public static double growthCoefficientStart = 0.9;
    public static double growthCoefficientMultiplier = 0.6;
    public static double growthCoefficientBottomLimit = 0.1;
    public final static int neuronsAtStart = 10;
    public final static int defaultWeight = 0;
    public static double underestimateErrorMultiplier = 1;
    public static double overestimateErrorMultiplier = 1;
    public static int reactionTreshold = 10000;
    public static double sectorSideThreshold = 0.3;
    public static int tailDrawRadius = 2;

    public final static String path = Environment.getExternalStorageDirectory().toString() + "/Recognizer/";
    private static String learningName = "alphabet";
    private static String workName = "test170";

    // debug
    public final static boolean debugSegmentation = false;
    public final static boolean debugNeuron = false;
    public final static boolean debugKohonen = false;
    public final static boolean debugTails = false;
    public final static boolean debugPaths = true;
    public final static boolean debugNetworkManager = true;

    // tails
    public final static double tresholdOfBlack = 0.5;
    public final static double tresholdOfBlackForDirectedInvestigation= 0.3;
    public final static double tailMinTresholdOfBlack = 0.4;
    public final static int tailMinBlacksInArea = 2;
    public final static double tailTopLimitOfBlackForPerimeter = 0.3;
    public final static double tailBottomLimitOfBlackForPerimeter = 0.1;
    public final static double tailBorderContainsAtleastBlacksFromTotal = 0.9;
    public static int maxTails = 5;
    public static int tailMargin = 4;
    public static int tailCheckTreshold = 16000;
    public final static int segmentBordersWidth = 2;
    public final static int unsuitableTailScores = -1;
    public final static int tailsBordersWidth = 1;
    public static int tailReactionTreshold = 15000;

    // paths
    public static double pathAngleTreshold = 60;
    public final static int unsuitablePathScores = -1;
    public static int pathReactionTreshold = 15000;


    private final static boolean useImageSizeFactor = true;
    private final static int imageSizeFactor = 2;
    private final static Size imageSize = new Size(768, 1024);
    private static Size realImageSize;
    private static String adjustedFilename;

    public static String getWorkName() {
        return workName;
    }

    public static void setWorkName(String workName) {
        Config.workName = workName;
    }

    public static String getLearningName() {
        return learningName;
    }

    public static void setLearningName(String learningName) {
        Config.learningName = learningName;
    }

    public final static String getName() {
        return (learningMode ? learningName : workName);
    }

    public final static String getOriginalFilename() {
        return path + getName() + ".jpg";
    }

    public final static String getAdjustedFilename() { return path + getName() + "_adj.jpg"; }

    public final static String getFilteredFilename() {
        return path + getName() + "_fil.jpg";
    }

    public final static String getSegmentedFilename() {
        return path + getName() + "_seg.jpg";
    }

    public final static String getProcessedFilename() {
        return path + getName() + "_proc.jpg";
    }

    public final static String getPiecesPath() {
        return path + getName() + "/";
    }

    public final static String getPieceFilename(int number, int line) {
        return getPiecesPath() + "line" + "_" + addNulls(line) + "/" + addNulls(number) +  ".jpg";
    }

    public final static String getNetworkFilename(String network) {
        return path + network + ".network";
    }

    public final static char getLearningChar(int number) throws Exception {
        if (number >= learningChars.length) {
            throw new Exception("Learning error: lack of learning chars (" + learningChars.length + ")");
        }

        return learningChars[number];
    }

    private static String addNulls(int value) {
        return String.format("%04d", value);
    }

    public static void setRealImageSize(Size imageSize) {
        Config.realImageSize = imageSize;
    }

    public static Size getRealImageSize() {
        return Config.realImageSize;
    }

    public static int getImageSizeWidth() {
        if (Config.useImageSizeFactor) {
            return Config.realImageSize.width / Config.imageSizeFactor;
        }
        else {
            return Config.imageSize.width;
        }
    }

    public static int getImageSizeHeight() {
        if (Config.useImageSizeFactor) {
            return Config.realImageSize.height / Config.imageSizeFactor;
        }
        else {
            return Config.imageSize.height;
        }
    }
}