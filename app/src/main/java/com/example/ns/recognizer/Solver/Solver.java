package com.example.ns.recognizer.Solver;

import com.example.ns.recognizer.Utils.RecognizerException;

public class Solver {
    enum Priority {
        Default,
        PlusMinus,
        MulDiv
    }

    class Position {
        int pos = 0;

        void reset() {
            pos = 0;
        }

        void increment() {
            pos++;
        }

        void shift(int offset) {
            pos += offset;
        }

        int get() {
            return pos;
        }
    }

    public String solve(String line) {
        if (line.isEmpty()) {
            throw new RecognizerException("Line is empty");
        }

        if (!line.endsWith("=")) {
            throw new RecognizerException("Line doesn't contain \'=\' char: " + line);
        }

        System.out.println("Solve: " + line);
        return solveRecursive(line, new Position(), Priority.Default);
    }

    private String solveRecursive(String line, Position index, Priority priority) {
        String result = takeNumber(line, index);
        while (true) {
            char currentOperation = line.charAt(index.get());

            if (shouldStop(priority, currentOperation)) {
                break;
            }

            index.increment();

            String number = solveRecursive(line, index, getOperationPriority(currentOperation));
            result = calc(result, currentOperation, number);
        }
        return result;
    }

    private boolean shouldStop(Priority priority, char currentOperation) {
        return !validPriority(priority, getOperationPriority(currentOperation))
                || terminateOperation(currentOperation);
    }

    private boolean validPriority(Priority priority, Priority nextPriority) {
        return (priority != nextPriority)
                && !(Priority.MulDiv == priority && Priority.PlusMinus == nextPriority);
    }

    private Priority getOperationPriority(char operation) {
        return ('*' == operation || '/' == operation ? Priority.MulDiv : Priority.PlusMinus);
    }

    private boolean terminateOperation(char operation) {
        if ('=' == operation) {
            return true;
        }

        if ('+' != operation
            && '-' != operation
            && '*' != operation
            && '/' != operation) {
            throw new RecognizerException("Undefined operation: " + operation);
        }
        return false;
    }

    private void checkNumber(String number) {
        if (null == number || number.isEmpty()) {
            throw new RecognizerException("Number's missing");
        }
    }

    private String takeNumber(String line, Position index) {
        int start = index.get();
        int end = start;
        while (Character.isDigit(line.charAt(end++)));

        index.shift(end - start - 1);

        String result = line.substring(start, end - 1);
        checkNumber(result);

        return result;
    }

    private String calc(String lhs, char operation, String rhs) {
        int lhsValue = Integer.parseInt(lhs);
        int rhsValue = Integer.parseInt(rhs);

        int result = 0;
        switch (operation) {
            case '+':
                result = lhsValue + rhsValue;
                break;
            case '-':
                result = lhsValue - rhsValue;
                break;
            case '*':
                result = lhsValue * rhsValue;
                break;
            case '/':
                result = lhsValue / rhsValue;
                break;
            default:
                throw new RecognizerException("Unknown operation: " + operation);
        }

        return String.valueOf(result);
    }
}