package com.example.ns.recognizer.Segment;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Segment.Objects.Findings;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.FileManager;
import com.example.ns.recognizer.Utils.Painter;

import java.io.IOException;

public class Segmenter {
    private final FileManager fileManager = new FileManager();
    private final Painter painter = new Painter();
    private final Investigator investigator = new Investigator();

    public Findings segment() throws IOException {
        System.out.println("###d segmenting started");

        Bitmap bitmap = fileManager.openBitmap(Config.getFilteredFilename());
        Findings findings = investigator.execute(bitmap);

        Bitmap colouredBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        painter.drawBorders(colouredBitmap, findings);
        fileManager.saveBitmap(colouredBitmap, Config.getSegmentedFilename());

        System.out.println("###d segmenting done");

        return findings;
    }
}