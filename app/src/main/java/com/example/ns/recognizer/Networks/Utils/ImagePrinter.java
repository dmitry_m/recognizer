package com.example.ns.recognizer.Networks.Utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.ns.recognizer.Networks.Kohonen.Kohonen;
import com.example.ns.recognizer.Networks.Neurons.Neuron;
import com.example.ns.recognizer.Segment.Objects.Line;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.FileManager;
import com.example.ns.recognizer.Utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

public class ImagePrinter {
    private final FileManager fileManager = new FileManager();

    public ImagePrinter() {
    }

    public void printAnswerOnImage(Bitmap bitmap, Line line, String answer, Kohonen network) throws IOException {
        ArrayList<SymbolArea> areas = line.getAreas();

        int averageDistance = 0;
        int averageOffsetTop = areas.get(0).top;
        int averageWidth = areas.get(0).width();
        int averageHeight = areas.get(0).height();
        for (int i = 1; i < areas.size(); ++i) {
            SymbolArea area = areas.get(i);
            SymbolArea previousArea = areas.get(i - 1);

            averageDistance += (area.centerX() - previousArea.centerX());
            averageOffsetTop += area.top;
            averageWidth += area.width();
            averageHeight += area.height();
        }

        averageDistance /= (areas.size() - 1);
        averageOffsetTop /= areas.size();
        averageWidth /= areas.size();
        averageHeight /= areas.size();

        int startX = areas.get(areas.size() - 1).right + averageDistance;

        printAt(bitmap, answer, network, startX, averageDistance, averageOffsetTop, averageWidth, averageHeight);
    }

    private void printAt(Bitmap bitmap, String answer, Kohonen network, int startX, int averageDistance, int averageOffsetTop, int averageWidth, int averageHeight) throws IOException {
        Canvas canvas = new Canvas(bitmap);

        for (int i = 0; i < answer.length(); ++i) {

            char symbol = answer.charAt(i);
            ArrayList<Neuron> suitableNeurons = network.getNeuronsWithSymbol(symbol);
            int chosenNeuronIndex = (int) (Math.random() * suitableNeurons.size());

            int offsetX = startX + (averageDistance + averageWidth) * i;
            int offsetY = averageOffsetTop;
            SymbolArea area = new SymbolArea(offsetX, offsetY, offsetX + averageWidth, offsetY + averageHeight);

            if (!Utils.isValidBound(area, bitmap)) {
                return;
            }

            Bitmap fragment = Utils.extractCellFromBitmap(bitmap, area);
            Neuron neuron = suitableNeurons.get(chosenNeuronIndex);
            int[][] weights = neuron.getWeights();

            Utils.resizeBitmap(fragment, Config.cellX, Config.cellY);
            impose(fragment, weights);
            fragment = Utils.resizeBitmap(fragment, area.width(), area.height());

            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setDither(true);
            paint.setFilterBitmap(true);
            canvas.drawBitmap(fragment, area.left, area.top, paint);
        }
    }

    private void impose(Bitmap fragment, int[][] weights) {
        for (int y = 0; y < Config.cellY; ++y) {
            for (int x = 0; x < Config.cellX; ++x) {
                double factor = (weights[x][y] / 255.0);
                int color = getModifiedPixel(fragment.getPixel(x, y), factor);
                fragment.setPixel(x, y, color);
            }
        }
    }

    private int getModifiedPixel(int pixel, double factor) {
        int argb = pixel;
        int r = (argb) & 0xFF;
        int g = (argb >> 8) & 0xFF;
        int b = (argb >> 16) & 0xFF;
        int a = (argb >> 24) & 0xFF;

        r -= (int) (r * factor);
        g -= (int) (g * factor);
        b -= (int) (b * factor);

        return Color.argb(a, r, g, b);
    }
}