package com.example.ns.recognizer.Networks.Kohonen;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Networks.Neurons.PathNeuron;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dima on 07.02.2015.
 */
public class KohonenPath implements INetwork {
    private final String name = "kohonen paths";
    private ArrayList<PathNeuron> neurons;
    private HashMap<Character, ArrayList<PathNeuron>> neuronsByGroup;

    public KohonenPath() {
        reset();
    }

    @Override
    public void reset() {
        this.neurons = new ArrayList<>(Config.neuronsAtStart);
        for (int i = 0; i < Config.neuronsAtStart; ++i) {
            neurons.add(new PathNeuron());
        }

        neuronsByGroup = new HashMap<>();
    }

    @Override
    public void learn(Bitmap bitmap, SymbolArea area, char symbol) {
        if (0 == neurons.size()) {
            return;
        }

        int minIndex = -1;
        double minReaction = -1;
        String line = "";

        for (int i = 0; i < neurons.size(); ++i) {
            int reaction = neurons.get(i).react(area);
            line += Utils.neuronReactionToStr(reaction) + " ";
            if (reaction >= 0 && (minReaction < 0 || reaction < minReaction)) {
                minIndex = i;
                minReaction = reaction;

                if (0 == minReaction) {
                    break;
                }
            }
        }

        if (minReaction < 0 || minReaction > Config.pathReactionTreshold) {
            neurons.add(new PathNeuron());
            line += String.format("%2d ", 0);
            minIndex = neurons.size() - 1;
        }

        PathNeuron neuron = neurons.get(minIndex);
        int oldReaction = (int)(neuron.react(area) / 1000);
        neuron.evolve(area, symbol);
        addNeuronToGroup(symbol, neuron);
        int newReaction = (int)(neuron.react(area) / 1000);


        String anglesStr = "[ ";
        for (Double angle : area.getAngles()) {
            anglesStr += angle + " ";
        }
        anglesStr += "]{";
        for (Double angle : neuron.getAngles()) {
            anglesStr += angle + " ";
        }
        anglesStr += "}";

        if (Config.debugPaths) {
            String reactionStr = String.format("%2d -> %2d",  oldReaction, newReaction);
            String okNotOkStr = (symbol == neuron.getSymbol() ? "[  ok  ]" : "[not ok]");
            String neuronChar = String.valueOf(neuron.getSymbol());
            System.out.println("all: " + line + ". neuron " + minIndex + "(" + neuronChar + "): " + reactionStr + ".  " + anglesStr + " " + okNotOkStr);
        }
    }

    private void addNeuronToGroup(char symbol, PathNeuron neuron) {
        if (neuronsByGroup.containsKey(symbol)) {
            neuronsByGroup.get(symbol).add(neuron);
        }
        else {
            neuronsByGroup.put(symbol, new ArrayList<PathNeuron>());
        }
    }

    @Override
    public ArrayList<Integer> recognize(Bitmap bitmap, SymbolArea area) {
        ArrayList<Integer> probabilities = new ArrayList<>();

        ArrayList<Double> rectDoubles = area.getAngles();
        for (int i = 0; i < neurons.size(); ++i) {
            probabilities.add((int)neurons.get(i).react(area));
        }
        return  probabilities;
    }

    @Override
    public void printNeuronsHeaders() {
        String line = "";
        String numbers = "";
        int number = 0;
        for (PathNeuron neuron : neurons) {
            line += String.format("%-2c", neuron.getSymbol()) + "  ";
            numbers += String.format("%-2d", number++) + "  ";
        }

        System.out.println("N           " + numbers);
        System.out.println("Neurons     " + line);
    }

    @Override
    public char getSymbolByNeuronIndex(int index) {
        return neurons.get(index).getSymbol();
    }

    @Override
    public void serialize() {
        try {
            FileOutputStream fos = new FileOutputStream(Config.getNetworkFilename(name));
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(neurons);
            oos.writeObject(neuronsByGroup);

            oos.close();
            fos.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public void deserialize() {
        try {
            FileInputStream fis = new FileInputStream(Config.getNetworkFilename(name));
            ObjectInputStream ois = new ObjectInputStream(fis);

            neurons = (ArrayList<PathNeuron>) ois.readObject();
            neuronsByGroup = (HashMap<Character, ArrayList<PathNeuron>>) ois.readObject();

            ois.close();
            fis.close();
        } catch(Exception ioe) {
            ioe.printStackTrace();
        }
    }
}
