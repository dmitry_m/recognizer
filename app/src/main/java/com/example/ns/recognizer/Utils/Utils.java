package com.example.ns.recognizer.Utils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;

import com.example.ns.recognizer.Activities.MainActivity;
import com.example.ns.recognizer.Networks.Neurons.Tail;
import com.example.ns.recognizer.Segment.Objects.Area;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

/**
 * Created by dima on 01.02.2015.
 */
public class Utils {
    private static final FileManager fileManager = new FileManager();

    public static int getPixelPower(Bitmap bitmap, int x, int y) {
        return (255 - bitmap.getPixel(x, y) & 0xff);
    }

    public static boolean isPixelWhite(int x, int y, Bitmap bitmap) {
        return !isPixelBlack(x, y, bitmap);
    }

    public static boolean isPixelWhite(Point point, Bitmap bitmap) {
        return Utils.isPixelWhite(point.x, point.y, bitmap);
    }

    public static boolean isPixelBlack(int x, int y, Bitmap bitmap) {
        return (bitmap.getPixel(x, y) & 0x00FFFFFF) < 0x00800000;
    }

    public static boolean isPixelBlack(Point point, Bitmap bitmap) {
        return Utils.isPixelBlack(point.x, point.y, bitmap);
    }

    public static Bitmap extractCellFromBitmap(Bitmap bitmap, SymbolArea area) {
        Bitmap cropped = Bitmap.createBitmap(bitmap, area.left, area.top, area.width(), area.height());
        return Utils.resizeBitmap(cropped, Config.cellX, Config.cellY);
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int degree) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Matrix matrix = new Matrix();
        matrix.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, false);
    }

    public static boolean isValidBound(SymbolArea area, Bitmap bitmap) {
        return (area.left >= 0
                && area.top >= 0
                && area.right < bitmap.getWidth()
                && area.bottom < bitmap.getHeight());
    }

    public static void removeDirectory(String path) {
        deleteRecursive(new File(path));
    }

    private static void deleteRecursive(File file) {
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                deleteRecursive(child);
            }
        }
        file.delete();
    }

    public static void dialogBox(MainActivity mainActivity) {
        AlertDialog.Builder alertDialogBuilder = new  AlertDialog.Builder(mainActivity);
        alertDialogBuilder.setMessage("Click on Image for tag");
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        alertDialogBuilder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static String generatePhotoName() {
        return "images/img " + new Timestamp(System.currentTimeMillis());
    }

    public static Bitmap adjustBitmap(final Bitmap bitmap) throws IOException {
        Bitmap result = bitmap;

        if (bitmap.getHeight() < bitmap.getWidth()) {
            result = Utils.rotateBitmap(bitmap, 90);
        }
        return Utils.resizeBitmap(result, Config.getImageSizeWidth(), Config.getImageSizeHeight());
    }

    public static Point coordinates2Sector(int x, int y, Area area) {
        int offsetX = x - area.left;
        int offsetY = y - area.top;
        return new Point(offsetX * Config.cellX / area.width(),
                offsetY * Config.cellY / area.height());
    }

    public static Point coordinates2Sector(Point p, Area area) {
        return coordinates2Sector(p.x, p.y, area);
    }

    public static Point sector2Coordinates(int x, int y, Area area) {
        int offsetX = x * area.width() / Config.cellX;
        int offsetY = y * area.height() / Config.cellY;
        return new Point(offsetX + area.left,
                offsetY + area.top);
    }

    public static Point sector2Coordinates(Point p, Area area) {
        return sector2Coordinates(p.x, p.y, area);
    }

    public static Tail rect2Tail(SymbolArea area, int index) {
        return new Tail(area, index);
    }

    public static String neuronReactionToStr(double reaction) {
        return String.format("%2d", (reaction >= 0 ? (int) reaction / 1000 : -1));
    }

    public static double getXFactor(SymbolArea area, int index) {
        Rect rect = area.getTails().get(index);
        int offsetX = rect.centerX() - area.left;
        return (double) offsetX / area.width();
    }

    public static double getYFactor(SymbolArea area, int index) {
        Rect rect = area.getTails().get(index);
        int offsetY = rect.centerY() - area.top;
        return (double) offsetY / area.height();
    }

    public static double distance(Point point1, Point point2) {
        double dx = point2.x - point1.x;
        double dy = point2.y - point1.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public static double angle(Point prev, Point curr, Point next) {
        double edgeToPrev = Utils.distance(prev, curr);
        double edgeToNext = Utils.distance(curr, next);
        double edgePrevToNext = Utils.distance(prev, next);

        return  Math.acos((edgeToPrev * edgeToPrev + edgeToNext * edgeToNext - edgePrevToNext * edgePrevToNext)
                / (2 * edgeToPrev * edgeToNext));
    }

    public static double delta(double angle1, double angle2) {
        return ((angle2 - angle1) % 180);
    }
}
