package com.example.ns.recognizer.Segment;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.example.ns.recognizer.Segment.Objects.TailSearchResult;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Utils;

import java.util.ArrayList;

public class SegmentChecker {
    public SegmentChecker() {
    }

    public int getAmountOfBlack(final int left, final int top, final int right, final int bottom, Bitmap bitmap) {
        int blacks = 0;
        for (int x = left; x < right; ++x) {
            for (int y = top; y < bottom; ++y) {
                if (Utils.isPixelBlack(x, y, bitmap)) {
                    blacks++;
                }
            }
        }

        return blacks;
    }

    public int getAmountOfBlack(final Rect rect, Bitmap bitmap) {
        return getAmountOfBlack(rect.left, rect.top, rect.right, rect.bottom, bitmap);
    }

    public boolean isEnoughOfBlack(final int left, final int top, final int right, final int bottom, Bitmap bitmap, double tresholdOfBlack) {
        double whiteInArea = 0;
        double delta = 1.0 / ((right - left) * (bottom - top));
        double tresholdOfWhite = 1.0 - tresholdOfBlack;
        for (int x = left; x < right; ++x) {
            for (int y = top; y < bottom; ++y) {
                if (Utils.isPixelWhite(x, y, bitmap)) {
                    whiteInArea += delta;
                }

                if (whiteInArea > tresholdOfWhite) {
                    return false;
                }
            }
        }

        return true;
    }

    public boolean isEnoughOfBlack(Rect rect, Bitmap bitmap) {
        return isEnoughOfBlack(rect.left, rect.top, rect.right, rect.bottom, bitmap, Config.tresholdOfBlack);
    }

    public boolean isEnoughOfBlack(Rect rect, Bitmap bitmap, double tresholdOfBlack) {
        return isEnoughOfBlack(rect.left, rect.top, rect.right, rect.bottom, bitmap, tresholdOfBlack);
    }

    public boolean isEnoughOfBlack(final int cx, final int cy, Bitmap bitmap) {
        return isEnoughOfBlack(cx - Config.radius, cy - Config.radius, cx + Config.radius, cy + Config.radius, bitmap, Config.tresholdOfBlack);
    }

    public boolean isEnoughOfBlack(final int cx, final int cy, Bitmap bitmap, double tresholdOfBlack) {
        return isEnoughOfBlack(cx - Config.radius, cy - Config.radius, cx + Config.radius, cy + Config.radius, bitmap, tresholdOfBlack);
    }

    public TailSearchResult isTail(Rect sectorRect, Bitmap bitmap) {
        boolean longestLengthIsOk = true;
        boolean longestContainsEnough = true;
        boolean enoughOfBlacks = true;

        int totalBlacksInBorder = 0;


        ArrayList<Integer> sides = new ArrayList<>();
        totalBlacksInBorder += obtainBlackSequence(sides, sectorRect.left, sectorRect.top, 1, 0, sectorRect.width(), bitmap);
        totalBlacksInBorder += obtainBlackSequence(sides, sectorRect.right, sectorRect.top, 0, 1, sectorRect.height(), bitmap);
        totalBlacksInBorder += obtainBlackSequence(sides, sectorRect.right, sectorRect.bottom, -1, 0, sectorRect.width(), bitmap);
        totalBlacksInBorder += obtainBlackSequence(sides, sectorRect.left, sectorRect.bottom, 0, -1, sectorRect.height(), bitmap);

        int longest = getLongestSequenceSize(sides);

        final int perimeter = (sectorRect.width() * 2 + sectorRect.height() * 2);
        final int blackPixelsTopLimit = (int) (Config.tailTopLimitOfBlackForPerimeter * perimeter);
        final int blackPixelsBottomLimit = (int) (Config.tailBottomLimitOfBlackForPerimeter * perimeter);
        if (longest == 0
            //|| longest < blackPixelsBottomLimit
              || longest > blackPixelsTopLimit) {
            longestLengthIsOk = false;
        }

        double shouldContainTreshold = totalBlacksInBorder * Config.tailBorderContainsAtleastBlacksFromTotal;
        if ((double)longest < shouldContainTreshold) {
            longestContainsEnough = false;
        }

        final int insetValueForRectWithoutBorders = 1;
        Rect rectInTheArea = new Rect(sectorRect);
        rectInTheArea.inset(insetValueForRectWithoutBorders, insetValueForRectWithoutBorders);

        int minSide = (rectInTheArea.width() < rectInTheArea.height() ? rectInTheArea.width() : rectInTheArea.height());
        int maxBlacksInArea = rectInTheArea.width() * rectInTheArea.height();
        int expectedBlacksInTheArea = (int)(longest * minSide * Config.tailMinTresholdOfBlack);
        expectedBlacksInTheArea = (expectedBlacksInTheArea < Config.tailMinBlacksInArea ? Config.tailMinBlacksInArea : expectedBlacksInTheArea);
        if (expectedBlacksInTheArea > maxBlacksInArea) {
            expectedBlacksInTheArea = maxBlacksInArea;
        }

        int blacksInTheArea = getAmountOfBlack(rectInTheArea, bitmap);
        if (blacksInTheArea < expectedBlacksInTheArea) {
            enoughOfBlacks = false;
        }

        if (Config.debugSegmentation)
        {
            System.out.println("try " + sectorRect + ". blacks in perimeter: " + totalBlacksInBorder
                    + ". top limit: " + blackPixelsTopLimit + " (got " + longest + ").  "
                    + "should contain: " + shouldContainTreshold + " (got " + longest + "). "
                    + "by " + (longest > minSide ? "total" : "minside") + " square. "
                    + "expected blacks: " + expectedBlacksInTheArea + " (got " + blacksInTheArea + ") "
                    + "sides: " + sides);
        }

        if (!longestLengthIsOk || !longestContainsEnough || !enoughOfBlacks) {
            return new TailSearchResult();
        }

        return new TailSearchResult(longest);
    }

    private int getLongestSequenceSize(ArrayList<Integer> sides) {
        // sequence: 16    5 0 5    0 8

        int longest = 0;
        int current = 0;
        int limit = sides.size();
        for (int i = 0; i < limit; ++i) {
            int index = i % sides.size();
            int blackDots = sides.get(index);

            if (blackDots == 0) {
                current = 0;

                if (limit == sides.size()) {
                    // when meet first '0' need to consider leading 16 and 5
                    limit += i;
                }
            }
            else {
                current += blackDots;
                if (current > longest) {
                    longest = current;
                }
            }
        }
        return longest;
    }

    private int obtainBlackSequence(ArrayList<Integer> sides, int cx, int cy, int dx, int dy, int distance, Bitmap bitmap) {
        boolean wasBlack = true;
        int dots = 0;
        int blacksTotal = 0;
        for (int offset = 0; offset < distance; ++offset) {
            int x = cx + offset * dx;
            int y = cy + offset * dy;

            boolean nowIsBlack = Utils.isPixelBlack(x, y, bitmap);
            if (nowIsBlack) {
                ++blacksTotal;
            }

            if (nowIsBlack != wasBlack) {
                if (dots > 0) {
                    sides.add(wasBlack ? dots : 0);
                }
                dots = 0;
                wasBlack = nowIsBlack;
            }
            ++dots;
        }
        sides.add(wasBlack ? dots : 0);

        return blacksTotal;
    }
}