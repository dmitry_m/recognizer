package com.example.ns.recognizer.Utils;

/**
 * Created by dima on 29.03.2015.
 */
public class Size {
    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int width;
    public int height;
}
