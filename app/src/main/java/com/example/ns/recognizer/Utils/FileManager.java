package com.example.ns.recognizer.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileManager {
    public Bitmap openBitmap(String filename) {
        return BitmapFactory.decodeFile(filename);
    }

    public void saveBitmap(Bitmap bitmap, String filename) throws IOException {
        File file = new File(filename);

        File fileDir = new File(file.getAbsoluteFile().getParentFile().getAbsolutePath());
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        FileOutputStream fos = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        fos.flush();
        fos.close();
    }


}