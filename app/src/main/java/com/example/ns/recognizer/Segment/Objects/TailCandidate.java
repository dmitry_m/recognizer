package com.example.ns.recognizer.Segment.Objects;

import android.graphics.Rect;

/**
 * Created by dima on 27.05.2015.
 */
public class TailCandidate implements Comparable<TailCandidate>{
    public int longestBorder;
    public Rect sector;

    public TailCandidate(int longestBorder, Rect sector) {
        this.longestBorder = longestBorder;
        this.sector = sector;
    }

    @Override
    public int compareTo(TailCandidate other) {
        return (longestBorder - other.longestBorder);
    }

}
