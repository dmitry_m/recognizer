package com.example.ns.recognizer.Segment.Objects;

import android.graphics.Point;
import android.graphics.Rect;

/**
 * Created by dima on 21.05.2015.
 */
public class Area {
    public int left;
    public int top;
    public int right;
    public int bottom;

    public Area() {}

    public Area(int left, int top, int right, int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public Area(Area area) {
        if (area == null) {
            left = top = right = bottom = 0;
        }
        else {
            left = area.left;
            top = area.top;
            right = area.right;
            bottom = area.bottom;
        }
    }

    public final boolean isEmpty() {
        return left >= right || top >= bottom;
    }

    public final int width() {
        return right - left;
    }

    public final int height() {
        return bottom - top;
    }

    public final int centerX() {
        return (left + right) >> 1;
    }

    public final int centerY() {
        return (top + bottom) >> 1;
    }

    public boolean contains(int x, int y) {
        return left < right && top < bottom  // check for empty first
                && x >= left && x < right && y >= top && y < bottom;
    }

    public boolean contains(Point point) {
        return contains(point.x, point.y);
    }

    public boolean contains(int left, int top, int right, int bottom) {
        return this.left < this.right && this.top < this.bottom
                && this.left <= left && this.top <= top
                && this.right >= right && this.bottom >= bottom;
    }

    public boolean contains(Area area) {
        return this.left < this.right && this.top < this.bottom
                && left <= area.left && top <= area.top && right >= area.right && bottom >= area.bottom;
    }

    public boolean intersects(int left, int top, int right, int bottom) {
        return this.left < right && left < this.right && this.top < bottom && top < this.bottom;
    }

    public Rect toRect() {
        return new Rect(this.left, this.top, this.right, this.bottom);
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;
        return left == area.left && top == area.top && right == area.right && bottom == area.bottom;
    }
}
