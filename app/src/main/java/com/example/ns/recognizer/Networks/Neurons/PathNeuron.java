package com.example.ns.recognizer.Networks.Neurons;

import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dima on 07.02.2015.
 */
public class PathNeuron implements Serializable{
    private double[] angles;

    private boolean notSet = true;
    private char symbol;
    private double growthCoefficient = Config.growthCoefficientStart;

    private void initPoints(SymbolArea area) {
        ArrayList<Double> angles = area.getAngles();
        this.angles = new double[angles.size()];

        for (int i = 0; i < angles.size(); ++i) {
            this.angles[i] = angles.get(i);
        }
    }

    public int react(SymbolArea area) {
        if (!canReactTo(area)) {
            return Config.unsuitablePathScores;
        }

        int reaction = 0;
        ArrayList<Double> anglesForArea = area.getAngles();
        for (int i = 0; i < anglesForArea.size(); ++i) {
            double delta = Math.abs(anglesForArea.get(i) - this.angles[i]);
            reaction += delta * delta;
        }

        return reaction;
    }

    public void evolve(SymbolArea area, char symbol) {
        if (!canEvolveTo(area)) {
            return;
        }

        if (notSet) {
            initPoints(area);
            this.symbol = symbol;
            notSet = false;
        } else {
            ArrayList<Double> anglesForArea = area.getAngles();
            for (int i = 0; i < anglesForArea.size(); ++i) {
                double delta = Math.abs(anglesForArea.get(i) - this.angles[i]);
                this.angles[i] += delta * growthCoefficient;
            }

            updateGrowthCoefficient();
        }
    }

    public boolean canEvolveTo(SymbolArea area) {
        return notSet || (this.angles.length == area.getAngles().size());
    }

    public boolean canReactTo(SymbolArea area) {
        return canEvolveTo(area);
    }

    private void updateGrowthCoefficient() {
        if (growthCoefficient > Config.growthCoefficientBottomLimit) {
            growthCoefficient *= Config.growthCoefficientMultiplier;
        }
    }

    public char getSymbol() {
        return this.symbol;
    }

    public double[] getAngles() {
        return this.angles;
    }
}
