package com.example.ns.recognizer.Segment;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;

import com.example.ns.recognizer.Segment.Objects.Findings;
import com.example.ns.recognizer.Segment.Objects.Mask;
import com.example.ns.recognizer.Segment.Objects.PathPoint;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Segment.Objects.TailCandidate;
import com.example.ns.recognizer.Segment.Objects.TailSearchResult;
import com.example.ns.recognizer.Segment.Objects.WeightedPoint;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;


public class Investigator {
    private final Findings findings = new Findings();
    private final Mask mask = new Mask();
    private final Mask tailsMask = new Mask();

    private final SegmentChecker segmentChecker = new SegmentChecker();

    public Findings execute(Bitmap bitmap) {
        this.reset(bitmap);

        for (int x = Config.radius; x < bitmap.getWidth() - Config.radius; x += Config.stepX) {
            for (int y = Config.radius; y < bitmap.getHeight() - Config.radius; y += Config.stepY) {
                if (segmentChecker.isEnoughOfBlack(x, y, bitmap)) {
                    investigate(x, y, bitmap);
                }
            }
        }

        return findings;
    }

    void investigate(final int cx, final int cy, Bitmap bitmap) {
        SymbolArea area = new SymbolArea(cx - Config.radius, cy - Config.radius, cx + Config.radius, cy + Config.radius);

        if (mask.isUsed(area)) {
            return;
        }

        while (true) {
            SymbolArea newArea = new SymbolArea(area);

            investigateRight(bitmap, area, newArea);
            investigateTop(bitmap, area, newArea);
            investigateLeft(bitmap, area, newArea);
            investigateBottom(bitmap, area, newArea);

            if (area.equals(newArea)) {
                break;
            }

            area = newArea;
        }
        findTails(area, bitmap);
        findPaths(area, bitmap);

        addArea(area);
    }

    private void findTails(SymbolArea area, Bitmap bitmap) {
        final int sectorWidth = (int) (area.width() / Config.sectorXFactor);
        final int sectorHeight = (int) (area.height() / Config.sectorYFactor);
        final int stepX = 1; // (sectorWidth / 3 > 0 ? sectorWidth / 3 : 1);
        final int stepY = 1; //(sectorHeight / 3 > 0 ? sectorHeight / 3 : 1);

        ArrayList<TailCandidate> tails = new ArrayList<>();
        Rect tailZone = new Rect(area.left >= Config.tailMargin ? area.left - Config.tailMargin : area.left,
                area.top >= Config.tailMargin ? area.top - Config.tailMargin : area.top,
                area.right + Config.tailMargin < bitmap.getWidth() ? area.right + Config.tailMargin : area.right,
                area.bottom + Config.tailMargin < bitmap.getHeight() ? area.bottom + Config.tailMargin : area.bottom);
        for (int x = tailZone.left; x < tailZone.right - sectorWidth; x += stepX) {
            for (int y = tailZone.top; y < tailZone.bottom - sectorHeight; y += stepY) {
                Rect sectorRect = new Rect(x, y, x + sectorWidth, y + sectorHeight);
                TailSearchResult tailSearchResult = segmentChecker.isTail(sectorRect, bitmap);
                if (tailSearchResult.success) {
                    tails.add(new TailCandidate(tailSearchResult.longestBorder, sectorRect));
                }
            }
        }

        Collections.sort(tails);

        int toAdd = (tails.size() > Config.maxTails ? Config.maxTails : tails.size());
        tailsMask.reset(bitmap);
        for (int i = 0; i < tails.size(); ++i) {

            Rect sector = tails.get(i).sector;
            if (tailsMask.isUsed(sector)) {
                continue;
            }
            area.addTail(sector);
            tailsMask.use(sector);

            if (0 == --toAdd) {
                break;
            }
        }
    }

    private double estimatePath(Point start, Point finish) {
        double dx = finish.x - start.x;
        double dy = finish.y - start.y;

        return Math.sqrt(dx * dx + dy * dy);
    }

    private double estimatePath(WeightedPoint start, int dx, int dy, WeightedPoint finish) {
        double overcome = start.weight + Math.sqrt((double) (dx * dx + dy * dy));
        Point newPoint = new Point(start.point.x + dx, start.point.y + dy);

        return overcome + estimatePath(newPoint, finish.point);
    }

    private void findPaths(SymbolArea area, Bitmap bitmap) {
        if (area.getTails().size() != 2) {
            return;
        }

        Rect startTail = area.getTails().get(0);
        Rect finishTail = area.getTails().get(1);

        if (startTail.centerY() < finishTail.centerY()) {
            Rect temp = startTail;
            startTail = finishTail;
            finishTail = temp;
        }

        WeightedPoint start = new WeightedPoint(new Point(startTail.centerX(), startTail.centerY()), 0);
        WeightedPoint finish = new WeightedPoint(new Point(finishTail.centerX(), finishTail.centerY()), 0);
        finish.weight = estimatePath(start.point, finish.point);

        PathPoint pathHolder = getPath(start, finish, area, bitmap);
        if (null == pathHolder) {
            return;
        }

        ArrayList<Point> points = new ArrayList<>();
        PathPoint current = pathHolder;
        while (null != current) {
            points.add(current.point);

            if (current.hasPrevious()) {
                current = current.previous;
            }
        }

        ArrayList<Double> angles = new ArrayList<>();
        double previousAngle = 0;
        for (int i = 1; i < points.size() - 1; ++i) {
            double curAngle = Utils.angle(points.get(i - 1), points.get(i), points.get(i + 1));
            double delta = Utils.delta(previousAngle, curAngle);

            if (Math.abs(delta) > Config.pathAngleTreshold) {
                angles.add(delta);
            }
        }
        area.setAngles(angles);
    }

    private PathPoint getPath(WeightedPoint start, WeightedPoint finish, SymbolArea area, Bitmap bitmap) {
        boolean[][] isUsed = new boolean[area.width()][area.height()];

        PriorityQueue<PathPoint> queue = new PriorityQueue<>(10, new Comparator<PathPoint>() {
            @Override
            public int compare(PathPoint lhs, PathPoint rhs) {
                return Double.compare(lhs.weight, rhs.weight);
            }
        });

        queue.add(new PathPoint(start));

        while (!queue.isEmpty()) {
            PathPoint pathPoint = queue.element();
            queue.remove(pathPoint);

            if (pathPoint.equals(finish)) {
                return pathPoint;
            }

            addPathPointIfNeeded(queue, pathPoint, finish, -1, 0, area, bitmap, isUsed);
            addPathPointIfNeeded(queue, pathPoint, finish, 0, -1, area, bitmap, isUsed);
            addPathPointIfNeeded(queue, pathPoint, finish, 1, 0, area, bitmap, isUsed);
            addPathPointIfNeeded(queue, pathPoint, finish, 0, 1, area, bitmap, isUsed);
        }

        return null;
    }

    private void addPathPointIfNeeded(PriorityQueue<PathPoint> queue, PathPoint pathPoint, WeightedPoint finish, int dx, int dy, SymbolArea area, Bitmap bitmap, boolean[][] isUsed) {
        Point pointToCheck = new Point(pathPoint.point.x + dx, pathPoint.point.y + dy);

        if (!Utils.isPixelBlack(pointToCheck, bitmap)) {
            return;
        }

        if (isUsed[pointToCheck.x][pointToCheck.y]) {
            return;
        }

        if (!area.contains(pointToCheck)) {
            return;
        }

        PathPoint pointToAdd = new PathPoint(pointToCheck,
                estimatePath(pathPoint, dx, dy, finish),
                pathPoint);

        queue.add(pointToAdd);
    }


    private SymbolArea adjustAreaSize(final SymbolArea area) {
        SymbolArea adjustedSymbolArea = new SymbolArea(area);
        int width = area.width();
        int height = area.height();

        if (width != height) {
            if (width > height) {
                int centerY = area.centerY();
                adjustedSymbolArea.top = centerY - width / 2;
                adjustedSymbolArea.bottom = centerY + width / 2;
            }

            if (width < height) {
                int centerX = area.centerX();
                adjustedSymbolArea.left = centerX - height / 2;
                adjustedSymbolArea.right = centerX + height / 2;
            }
        }

        return adjustedSymbolArea;
    }

    void investigateTop(Bitmap bitmap, final SymbolArea area, SymbolArea newArea) {
        if (!checkBounds(bitmap, area)) {
            return;
        }

        int startX = area.left - Config.radius;
        int startY = area.top - Config.radius;
        int endX = area.right + Config.radius;
        int endY = startY;

        int stepX = Config.stepX;
        int stepY = 0;
        if (investigateInDirection(bitmap, startX, startY, endX, endY, stepX, stepY)) {
            newArea.top -= Config.radius;

            if (mask.isUsed(newArea)) {
                newArea.top = area.top;
            }
        }
    }

    void investigateBottom(Bitmap bitmap, final SymbolArea area, SymbolArea newArea) {
        if (!checkBounds(bitmap, area)) {
            return;
        }

        int startX = area.left - Config.radius;
        int startY = area.bottom + Config.radius;
        int endX = area.right + Config.radius;
        int endY = startY;

        int stepX = Config.stepX;
        int stepY = 0;
        if (investigateInDirection(bitmap, startX, startY, endX, endY, stepX, stepY)) {
            newArea.bottom += Config.radius;

            if (mask.isUsed(newArea)) {
                newArea.bottom = area.bottom;
            }
        }
    }

    void investigateLeft(Bitmap bitmap, final SymbolArea area, SymbolArea newArea) {
        if (!checkBounds(bitmap, area)) {
            return;
        }

        int startX = area.left - Config.radius;
        int startY = area.top - Config.radius;
        int endX = startX;
        int endY = area.bottom + Config.radius;

        int stepX = 0;
        int stepY = Config.stepY;
        if (investigateInDirection(bitmap, startX, startY, endX, endY, stepX, stepY)) {
            newArea.left -= Config.radius;

            if (mask.isUsed(newArea)) {
                newArea.left = area.left;
            }
        }
    }

    void investigateRight(Bitmap bitmap, final SymbolArea area, SymbolArea newArea) {
        if (!checkBounds(bitmap, area)) {
            return;
        }

        int startX = area.right + Config.radius;
        int startY = area.top - Config.radius;
        int endX = startX;
        int endY = area.bottom - Config.radius;

        int stepX = 0;
        int stepY = Config.stepY;
        if (investigateInDirection(bitmap, startX, startY, endX, endY, stepX, stepY)) {
            newArea.right += Config.radius;

            if (mask.isUsed(newArea)) {
                newArea.right = area.right;
            }
        }
    }

    boolean investigateInDirection(Bitmap bitmap, int startX, int startY, int endX, int endY, int stepX, int stepY) {
        final double tresholdOfBlack = Config.tresholdOfBlackForDirectedInvestigation;

        int x = startX;
        int y = startY;
        while (x <= endX && y <= endY) {
            if (segmentChecker.isEnoughOfBlack(x, y, bitmap, tresholdOfBlack)) {
                return true;
            }
            x += stepX;
            y += stepY;
        }

        return false;
    }

    boolean checkBounds(Bitmap bitmap, SymbolArea area) {
        return (area.left >= Config.diameter
                && area.right < bitmap.getWidth() - Config.diameter
                && area.left < area.right
                && area.top >= Config.diameter
                && area.bottom < bitmap.getHeight() - Config.diameter
                && area.top < area.bottom);
    }

    void addArea(SymbolArea area) {
        this.mask.use(area);
        this.findings.add(area);
    }

    public void reset(Bitmap bitmap) {
        this.findings.reset();
        this.mask.reset(bitmap);
        this.tailsMask.reset(bitmap);
    }
}