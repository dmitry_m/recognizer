package com.example.ns.recognizer.Networks.Neurons;

import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Utils;

import java.io.Serializable;

/**
 * Created by dima on 07.02.2015.
 */
public class Tail implements Serializable{
    public double x;
    public double y;
    private double growthCoefficient = Config.growthCoefficientStart;

    public Tail(SymbolArea area, int index) {
        this.x = Utils.getXFactor(area, index);
        this.y = Utils.getYFactor(area, index);
    }

    public int react(SymbolArea area, int index) {
        double otherX = Utils.getXFactor(area, index);
        double otherY = Utils.getYFactor(area, index);

        double dx = otherX - x;
        double dy = otherY - y;
        return (int) (Math.sqrt(dx * dx + dy * dy) * 10000);
    }

    public void evolve(SymbolArea area, int index) {
        double otherX = Utils.getXFactor(area, index);
        double otherY = Utils.getYFactor(area, index);

        double dx = otherX - x;
        double dy = otherY - y;

        x += dx * growthCoefficient;
        y += dy * growthCoefficient;

        updateGrowthCoefficient();
    }

    private void updateGrowthCoefficient() {
        if (growthCoefficient > Config.growthCoefficientBottomLimit) {
            growthCoefficient *= Config.growthCoefficientMultiplier;
        }
    }
}
