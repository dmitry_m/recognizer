package com.example.ns.recognizer.Activities;

import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Size;

public class CameraHandlingActivity extends ActionBarActivity {
    protected Camera camera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initCamera();
    }

    @Override
    protected void onStart() {
        super.onResume();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        System.out.println("###d DESTROYED");
        super.onDestroy();
    }

    public void initCamera(){
        try {
            camera = Camera.open();
            camera.setDisplayOrientation(90);
            adjustCameraParams();
        }
        catch (Exception e){
            System.out.println("Hasn't succeed with camera open");
            e.printStackTrace();
        }
    }

    private void releaseCamera() {
        if (null != camera) {
            camera.release();
        }
    }

    private void adjustCameraParams() {
        Camera.Parameters params = camera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        camera.setParameters(params);

        adjustConfigImageSize(params);
    }

    private void adjustConfigImageSize(Camera.Parameters params) {
        Camera.Size pictureSize = params.getPictureSize();
        Size validSize;
        if (pictureSize.width > pictureSize.height) {
            validSize = new Size(pictureSize.height, pictureSize.width);
        }
        else {
            validSize = new Size(pictureSize.width, pictureSize.height);
        }
        Config.setRealImageSize(validSize);
    }
}