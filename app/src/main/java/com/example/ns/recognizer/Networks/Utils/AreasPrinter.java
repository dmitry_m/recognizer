package com.example.ns.recognizer.Networks.Utils;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Segment.Objects.Findings;
import com.example.ns.recognizer.Segment.Objects.Line;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.FileManager;
import com.example.ns.recognizer.Utils.Utils;

import java.io.IOException;

/**
 * Created by dima on 07.02.2015.
 */
public class AreasPrinter {
    private static FileManager fileManager = new FileManager();

    private static void printLineImpl(Bitmap bitmap, Findings findings, int index) throws IOException {
        Line line = findings.getLines().get(index);

        int count = 0;
        for (SymbolArea area : line.getAreas()) {
            Bitmap resized = Utils.extractCellFromBitmap(bitmap, area);
            fileManager.saveBitmap(resized, Config.getPieceFilename(count++, index));
        }
    }

    public static void printLine(Bitmap bitmap, Findings findings, int index) throws IOException {
        Utils.removeDirectory(Config.getPiecesPath());

        printLineImpl(bitmap, findings, index);
    }

    public static void printAllLines(Bitmap bitmap, Findings findings) throws IOException {
        Utils.removeDirectory(Config.getPiecesPath());

        for (int i = 0; i < findings.getLines().size(); ++i) {
            printLineImpl(bitmap, findings, i);
        }
    }
}
