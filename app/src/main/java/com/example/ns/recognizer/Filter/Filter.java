package com.example.ns.recognizer.Filter;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.FileManager;
import com.example.ns.recognizer.Utils.Utils;

import java.io.IOException;

import Catalano.Imaging.Concurrent.Filters.BradleyLocalThreshold;
import Catalano.Imaging.FastBitmap;

public class Filter {
    private final FileManager fileManager = new FileManager();

    public void filter() throws IOException {
        System.out.println("###d filtering started");

        Bitmap bitmap = fileManager.openBitmap(Config.getOriginalFilename());
        bitmap = Utils.adjustBitmap(bitmap);
        fileManager.saveBitmap(bitmap, Config.getAdjustedFilename());

        FastBitmap fastBitmap = new FastBitmap(bitmap);
        fastBitmap.toGrayscale();
        BradleyLocalThreshold bradley = new BradleyLocalThreshold(256);
        bradley.applyInPlace(fastBitmap);

        fileManager.saveBitmap(fastBitmap.toBitmap(), Config.getFilteredFilename());

        System.out.println("###d filtering done");
    }
}