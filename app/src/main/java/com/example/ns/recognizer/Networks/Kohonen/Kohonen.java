package com.example.ns.recognizer.Networks.Kohonen;

import android.graphics.Bitmap;

import com.example.ns.recognizer.Networks.Neurons.Neuron;
import com.example.ns.recognizer.Segment.Objects.SymbolArea;
import com.example.ns.recognizer.Utils.Config;
import com.example.ns.recognizer.Utils.Utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by dima on 07.02.2015.
 */
public class Kohonen implements INetwork{
    private final String name = "kohonen";
    private ArrayList<Neuron> neurons;
    private HashMap<Character, ArrayList<Neuron>> neuronsByGroup;

    public Kohonen() {
        reset();
    }

    @Override
    public void reset() {
        this.neurons = new ArrayList<>(Config.neuronsAtStart);
        for (int i = 0; i < Config.neuronsAtStart; ++i) {
            neurons.add(new Neuron());
        }

        neuronsByGroup = new HashMap<>();
    }

    @Override
    public void learn(Bitmap bitmap, SymbolArea area, char symbol) {
        if (0 == neurons.size()) {
            return;
        }

        Bitmap croppedBitmap = Utils.extractCellFromBitmap(bitmap, area);

        int index = 0;
        double minReaction = neurons.get(0).react(croppedBitmap);
        String line = Utils.neuronReactionToStr(minReaction) + " ";

        for (int i = 1; i < neurons.size(); ++i) {
            double reaction = neurons.get(i).react(croppedBitmap);
            line += Utils.neuronReactionToStr(reaction) + " ";
            if (reaction < minReaction) {
                index = i;
                minReaction = reaction;
            }
        }

        if (minReaction > Config.reactionTreshold) {
            neurons.add(new Neuron());
            line += String.format("%2d ", 0);
            index = neurons.size() - 1;
        }

        Neuron neuron = neurons.get(index);
        int oldReaction = (int)(neuron.react(croppedBitmap) / 1000);
        neuron.evolve(croppedBitmap, symbol);
        addNeuronToGroup(symbol, neuron);
        int newReaction = (int)(neuron.react(croppedBitmap) / 1000);

        if (Config.debugKohonen) {
            String reactionStr = String.format("%2d -> %2d",  oldReaction, newReaction);
            String okNotOkStr = (symbol == neuron.getSymbol() ? "[  ok  ]" : "[not ok]");
            String neuronChar = String.valueOf(neuron.getSymbol());
            System.out.println("all: " + line + ". neuron " + index + "(" + neuronChar + "): " + reactionStr + ".  " + okNotOkStr);
        }
    }

    private void addNeuronToGroup(char symbol, Neuron neuron) {
        if (neuronsByGroup.containsKey(symbol)) {
            neuronsByGroup.get(symbol).add(neuron);
        }
        else {
            neuronsByGroup.put(symbol, new ArrayList<Neuron>());
        }
    }

    @Override
    public ArrayList<Integer> recognize(Bitmap bitmap, SymbolArea area) {
        ArrayList<Integer> probabilities = new ArrayList<>();

        for (int i = 0; i < neurons.size(); ++i) {
            probabilities.add((int)neurons.get(i).react(bitmap));
        }
        return  probabilities;
    }

    @Override
    public void printNeuronsHeaders() {
        String line = "";
        String numbers = "";
        int number = 0;
        for (Neuron neuron : neurons) {
            line += String.format("%-2c", neuron.getSymbol()) + "  ";
            numbers += String.format("%-2d", number++) + "  ";
        }

        System.out.println("N           " + numbers);
        System.out.println("Neurons     " + line);
    }

    @Override
    public char getSymbolByNeuronIndex(int index) {
        return neurons.get(index).getSymbol();
    }

    public ArrayList<Neuron> getNeuronsWithSymbol(char symbol) {
        return neuronsByGroup.get(symbol);
    }

    @Override
    public void serialize() {
        try {
            FileOutputStream fos = new FileOutputStream(Config.getNetworkFilename(name));
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(neurons);
            oos.writeObject(neuronsByGroup);

            oos.close();
            fos.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public void deserialize() {
        try {
            FileInputStream fis = new FileInputStream(Config.getNetworkFilename(name));
            ObjectInputStream ois = new ObjectInputStream(fis);

            neurons = (ArrayList<Neuron>) ois.readObject();
            neuronsByGroup = (HashMap<Character, ArrayList<Neuron>>) ois.readObject();

            ois.close();
            fis.close();
        } catch(Exception ioe) {
            ioe.printStackTrace();
        }
    }
}
